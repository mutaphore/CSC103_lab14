

public class RunTimes {

      public static void main(String[] args) {
         
         long startTime;
         long endTime;
         
         System.out.println("Logarithmic alogorithm's running times: ");
         
         for(long N=10000; N<100000000; N*=2) {
            
            startTime = System.nanoTime();
            Algorithms.logarithmicAlgTime(N);
            endTime = System.nanoTime();
            System.out.println("T(" + N + ") = " + (endTime-startTime)/1000000);
            
         }
         
         System.out.println("Linear alogorithm's running times: ");
         
         for(long N=10000; N<100000000; N*=2) {
            
            startTime = System.nanoTime();
            Algorithms.linearAlgTime(N);
            endTime = System.nanoTime();
            System.out.println("T(" + N + ") = " + (endTime-startTime)/1000000);
            
         }
         
         System.out.println("NlogN alogorithm's running times: ");

         for(long N=10000; N<100000000; N*=2) {
            
            startTime = System.nanoTime();
            Algorithms.NlogNAlgTime(N);
            endTime = System.nanoTime();
            System.out.println("T(" + N + ") = " + (endTime-startTime)/1000000);
            
         }
         
         System.out.println("Quadratic alogorithm's running times: ");
         
         for(long N=10000; N<400000; N*=2) {
            
            startTime = System.nanoTime();
            Algorithms.quadraticAlgTime(N);
            endTime = System.nanoTime();
            System.out.println("T(" + N + ") = " + (endTime-startTime)/1000000);
            
         }
         
      }
   
}


public class Algorithms {

      public static void linearAlgTime(long N) {
         
         long x;
         
         for(long i=1; i<=N; i++)
            x = i;
         
      }
      
      public static void quadraticAlgTime(long N) {
         
         long x;
         
         for(long i=1; i<=N; i++) {
            for(long j=1; j<=N; j++) {
               x = i;
            }            
         }
         
      }
      
      public static void logarithmicAlgTime(long N) {
         
         long x;
         
         for(long i=N; i>=1; i/=2)
            x = 1;
            
      }
      
      public static void NlogNAlgTime(long N) {
         
         long x;
         
         for(long i=1; i<=N; i++) {
            for(long j=N; j>=1; j/=2)
               x = 1;
         }
                  
      }
   
}
